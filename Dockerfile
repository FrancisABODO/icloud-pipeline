# Grab the latest Alpine image
FROM alpine:latest

# Install python, pip, and bash. No need for python3-venv since we're using venv from Python 3
RUN apk add --no-cache --update python3 py3-pip bash

ENV PORT=5000

# Setup virtual environment
RUN python3 -m venv /app/venv
# Activate virtual environment for subsequent commands
ENV PATH="/app/venv/bin:$PATH"

# Now it's safe to upgrade pip in the virtual environment
RUN pip install --upgrade pip

# Copy the requirements file into the container
COPY ./webapp/requirements.txt /tmp/requirements.txt

# Install dependencies using pip in the virtual environment
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# Add our code
COPY ./webapp /opt/webapp/
WORKDIR /opt/webapp

# Run the image as a non-root user
RUN adduser -D myuser
USER myuser

# CMD is required to run on Heroku
CMD ["gunicorn", "--bind", "0.0.0.0:$PORT", "wsgi"]